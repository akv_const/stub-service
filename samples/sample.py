import json

# request URI
if 'requestURI' not in locals():
    requestURI = '/api/v2/Orders/7E6BDA82-FF9F-4EF4-BE6A-91DA0667FD69/dss-parameters'
# request body
if 'requestBody' not in locals():
    requestBody = '{"formId": "4ba9b2fc-e119-4ccd-8d88-d134683bbee4"}'
# request params
if 'requestParams' not in locals():
    requestParams = '{}'

responses = {
    '4ba9b2fc-e119-4ccd-8d88-d134683bbee4': 'someValue string',
    '94c49609-5639-47cf-9250-61562e9ff266': '{"someValue": "2"}',
    '7E6BDA82-FF9F-4EF4-BE6A-91DA0667FD69': '{"askPin":true,"loginType":"Email","contactType":"Phone","secondFactor":{"type":"myDSS","onActions":["onLogin","onSign"]}}'
}

# response body must be returnet in 'response' variable
# http status code in 'statusCode' variable
try:
    response = responses[requestURI.split('/')[4]]
    statusCode = 200
except KeyError:
    statusCode = 404
    response = None
