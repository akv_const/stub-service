package ru.akv.stubservice

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class StubServiceApplication(
	@Value("\${config.path}") val configPath: String
) {

	@Bean
	fun getConfiguration() : Configuration {
		LoggerFactory.getLogger("ru.akv.stubservice.Application").info("Config: $configPath")
		return WatchableConfiguration(configPath)
	}
}

fun main(args: Array<String>) {
	runApplication<StubServiceApplication>(*args) {
		setBannerMode(Banner.Mode.OFF)
		setLogStartupInfo(false)
	}
	LoggerFactory.getLogger("ru.akv.stubservice.Application").info("Application started")
}
