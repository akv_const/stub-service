package ru.akv.stubservice

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.web.util.UriTemplate
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantReadWriteLock

data class ConfigElement(
    @JsonProperty("method")
    val method: String,
    @JsonProperty("path")
    val path: String,
    @JsonProperty("type")
    val type: Type,
    @JsonProperty("response")
    val response: JsonNode
) {
    enum class Type {
        @JsonProperty("static")
        STATIC,
        @JsonProperty("script")
        SCRIPT
    }
}

inline fun <L: Lock, R> locked(lock: L, block: () -> R): R {
    lock.lock()
    try {
        return block()
    }
    finally {
        lock.unlock()
    }
}

open class Configuration() {
    private var elements: MutableList<ConfigElement> = mutableListOf()
    private val elemsLock = ReentrantReadWriteLock()
    private val objectMapper = ObjectMapper()

    constructor(inputStream: InputStream) : this() {
        reload(inputStream)
    }

    fun reload(inputStream: InputStream) {
        locked(elemsLock.writeLock()) {
            val jsonReader = objectMapper.readerForListOf(ConfigElement::class.java)
            elements = jsonReader.readValue(inputStream)
        }
    }

    fun find(method: String, path: String): ConfigElement? {
        locked(elemsLock.readLock()) {
            return elements.find {
                it.method.equals(method, true) && UriTemplate(it.path.lowercase()).matches(path.lowercase())
            }
        }
    }

    open fun add(configElement: ConfigElement) {
        locked(elemsLock.writeLock()) {
            val prev = find(configElement.method, configElement.path)
            if (prev != null)
                elements.remove(prev)
            elements.add(configElement)
        }
    }

    open fun remove(method: String, path: String) {
        locked(elemsLock.writeLock()) {
            elements.removeAll {
                it.method.equals(method, true) && it.path.equals(path, true)
            }
        }
    }

    fun toJson(): JsonNode {
        locked(elemsLock.readLock()) {
            return objectMapper.valueToTree(elements)
        }
    }

    fun serialize(outputStream: OutputStream, isPrettyPrint: Boolean = false) {
        locked(elemsLock.readLock()) {
            if (isPrettyPrint)
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(outputStream, elements)
            else
                objectMapper.writeValue(outputStream, elements)
        }
    }
}

class WatchableConfiguration(configFileName: String): Configuration(FileInputStream(configFileName)) {
    private val log = LoggerFactory.getLogger(WatchableConfiguration::class.java)
    private val configPath = Path.of(configFileName).toAbsolutePath()
    private val watchService = FileSystems.getDefault().newWatchService()
    private var watchKey = configPath.parent.register(watchService, ENTRY_MODIFY)
    private val observerThread = Thread {
        while (true) {
            try {
                val key = watchService.take()
                key.pollEvents().forEach {
                    if (it.kind() == ENTRY_MODIFY) {
                        val path = it.context() as Path
                        if (path.equals(configPath.fileName)) {
                            log.info("Config was externally changed. Reloading.")
                            reload()
                        }
                    }
                }
                key.reset()
            }
            catch(ex: Exception) {
                log.error(ex.stackTraceToString())
            }
        }
    }

    init {
        observerThread.start()
    }

    private fun reload() {
        try {
            super.reload(FileInputStream(configPath.toFile()))
        }
        catch (ex: Exception) {
            log.error("Cannot reload config: {}", ex.stackTraceToString())
        }
    }

    @Synchronized
    private fun save() {
        try {
            watchKey.cancel()
            serialize(FileOutputStream(configPath.toFile()), true)
            watchKey = configPath.parent.register(watchService, ENTRY_MODIFY)
        }
        catch (ex: Exception) {
            log.error("Cannot save config: {}", ex.stackTraceToString())
        }
    }

    override fun add(configElement: ConfigElement) {
        super.add(configElement)
        log.info("Config was changed. Saving.")
        save()
    }

    override fun remove(method: String, path: String) {
        super.remove(method, path)
        log.info("Config was changed. Saving.")
        save()
    }
}
