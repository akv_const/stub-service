package ru.akv.stubservice

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.JsonNodeType
import java.io.FileReader
import javax.script.ScriptEngineManager
import javax.script.ScriptException

data class ResponseData(val statusCode: Int, val body: Any?)
class ConfigException(message: String, cause: Exception? = null) : Exception(message, cause)

fun getResponseForStatic(responseConfig: JsonNode): ResponseData {
    val statusCode = responseConfig.findPath("code")?.asInt(200)!!
    val nodeBody = responseConfig.findPath("body")
    return when (nodeBody?.nodeType) {
        null, JsonNodeType.NULL, JsonNodeType.MISSING -> ResponseData(statusCode, null)
        JsonNodeType.STRING -> ResponseData(statusCode, nodeBody.textValue())
        else -> ResponseData(statusCode, nodeBody)
    }
}

fun getResponseForScript(responseConfig: JsonNode, requestURI: String, requestBody: String?, requestParams: Map<String, String>): ResponseData {
    val lang = responseConfig.findPath("language").textValue() ?: throw ConfigException("В настройках не указан язык скрипта.")
    val source = responseConfig.findPath("source").textValue() ?: throw ConfigException("В настройках не указан путь к скрипту.")
    val scriptEngineManager = ScriptEngineManager()
    val scriptEngine = scriptEngineManager.getEngineByName(lang) ?: throw ConfigException("Не найден ScriptEngine для языка $lang")
    try {
        val bind = scriptEngine.createBindings()
        bind["requestURI"] = requestURI
        bind["requestBody"] = requestBody
        bind["requestParams"] = requestParams
        scriptEngine.eval(FileReader(source), bind)
        val statusCode = bind["statusCode"] as Int? ?: throw ConfigException("Скрипт не вернул статус ответа.")
        val response = bind["response"]
        if (response != null) {
            try {
                val objectMapper = ObjectMapper()
                val responseNode = objectMapper.readTree(response as String)
                if (responseNode.nodeType == JsonNodeType.OBJECT)
                    return ResponseData(statusCode, responseNode)
            } catch (ex: Exception) {
            }
        }
        return ResponseData(statusCode, response)
    } catch (ex: ScriptException) {
        throw ConfigException("Ошибка выполнения скрипта.", ex)
    }
}