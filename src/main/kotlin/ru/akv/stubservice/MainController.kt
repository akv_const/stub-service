package ru.akv.stubservice

import com.fasterxml.jackson.databind.JsonNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import javax.servlet.http.HttpServletRequest

@RestController
class MainController {
    val log: Logger = LoggerFactory.getLogger(MainController::class.java)
    @Autowired
    lateinit var configuration: Configuration

    @RequestMapping("/**")
    fun processAll(request: HttpServletRequest,
                   @RequestBody requestBody: String?,
                   @RequestParam requestParams: Map<String, String>): ResponseEntity<Any?> {
        log.info("Request: {} {}", request.method, request.requestURI)
        val config = configuration.find(request.method, request.requestURI)
        if (config == null) {
            log.warn("No config for {} {}", request.method, request.requestURI)
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Config not found")
        }
        log.debug("Found config: {}", config)
        try {
            val responseData = when (config.type) {
                ConfigElement.Type.STATIC -> getResponseForStatic(config.response)
                ConfigElement.Type.SCRIPT -> getResponseForScript(config.response, request.requestURI, requestBody, requestParams)
            }
            return ResponseEntity.status(responseData.statusCode).body(responseData.body)
        } catch (ex: Exception) {
            log.error("Не удалось получить данные ответа.", ex)
            throw ex
        }
    }

    @GetMapping("currentserviceconfiguration")
    fun getConfig(): JsonNode {
        log.info("Configuration requested")
        return configuration.toJson()
    }

    @PostMapping("currentserviceconfiguration")
    fun postConfig(@RequestBody configElement: ConfigElement) {
        log.info("Add configuration: {}", configElement.toString())
        configuration.add(configElement)
    }

    @DeleteMapping("currentserviceconfiguration")
    fun removeConfig(@RequestBody configElement: ConfigElement) {
        log.info("Remove configuration: {}", configElement.toString())
        configuration.remove(configElement.method, configElement.path)
    }

    @ExceptionHandler(ResponseStatusException::class)
    fun processResponseStatus(ex: ResponseStatusException): ResponseEntity<String> {
        return ResponseEntity.status(ex.status).body(ex.reason)
    }

    @ExceptionHandler(ConfigException::class)
    fun processConfigException(ex: ConfigException): ResponseEntity<String> {
        return ResponseEntity.internalServerError().body(ex.message)
    }
}